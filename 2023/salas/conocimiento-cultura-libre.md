---
layout: 2023/post
section: proposals
category: devrooms
author: Wikimedia España
title: Conocimiento y Cultura Libre
---

# Conocimiento y Cultura Libre

## Detalles de la propuesta:

>El propósito de esta sala es ofrecer y facilitar a la comunidad distintas opciones para colaborar en el movimiento por el conocimiento libre.

-   Formato:

>Sesiones específicas por determinar. En general, una duración de 3-4 horas, ya sea por la mañana o por la tarde.

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

## Comunidad que propone la sala:

### Wikimedia España

>Wikimedia España es el capítulo reconocido por la Fundación Wikimedia para operar en España. Somos una asociación sin ánimo de lucro y reconocida como entidad de utilidad pública que promueve el conocimiento libre y los proyectos Wikimedia, siendo Wikipedia el más conocido. Nos constituimos en 2011 y llevamos todos estos años trabajando para mejorar la accesibilidad, el uso y la participación en Wikipedia y sus proyectos hermanos tanto a nivel social como institucional.<br><br>
La visión del movimiento Wikimedia es conseguir un mundo en el que todas las personas tengan acceso libre a la suma del conocimiento y puedan participar en su construcción colectiva. Seguimos las filosofías del conocimiento y la cultura libres, que defienden el derecho fundamental de acceso a estos, y todos nuestros proyectos operan sobre plataformas de software libre.<br><br>
Todo el mundo tiene conocimiento para compartir. Desde escribir o mejorar un artículo en Wikipedia, a documentar manifestaciones culturales y subir las imágenes a Wikimedia Commons. Personas individuales, colectivos u organizaciones: todas pueden formar parte del ecosistema Wikimedia.

-   Web: <https://www.wikimedia.es>
-   Twitter: <https://twitter.com/@wikimedia_es>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
