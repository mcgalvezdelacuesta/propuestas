---
layout: 2023/post
section: proposals
category: devrooms
author: Geoinquietos & comunidad OpenStreetMap
title: LibreGeo&#58; Geoinquietos, OpenStreetMap, QGIS y otras aplicaciones libres espaciales
---

# LibreGeo: Geoinquietos, OpenStreetMap, QGIS y otras aplicaciones libres espaciales

## Detalles de la propuesta:

>El objetivo de esta sala es la de dar cabida a la cultura libre en torno a los datos espaciales: tanto (1) en la creación de datos espaciales libres, como es el de la comunidad de colaboradores de OpenStreetMap, como (2) los usuarios y desarrolladores de aplicaciones libres de geomáticas, como pueden ser QGIS u otras herramientas libres para el tratamiento de datos espaciales.

-   Formato:

>Charlas y mesas redondas por determinar en torno a la temática planteada con una duración de 3 a 4 horas en el segundo día del congreso (sábado).

-   Público objetivo:

>Personas interesadas en el tratamiento y la generación de datos espaciales en proyectos y herramientas libres.

<p style="padding-top: 5px; text-align: center; font-size: 1.5rem;"><strong>Pues enviar propuestas para esta sala desde <strong><a href="https://eslib.re/2023/enviar/sala-libregeo" target="_blank">esta página</a></strong> hasta el <span class="red">19 de marzo</span>.<br></strong></p>

## Comunidad que propone la sala:

### Geoinquietos & comunidad OpenStreetMap

>Geoinquietos son un conjunto de grupos que tienen su actividad en torno los datos y las herramientas libres de carácter espacial. Dentro de los diferentes grupos se promueven actividades para conversar y aprender sobre tecnologías de la información geográfica, los datos espaciales y otras cuestiones relacionadas con las ciencias para el estudio de la tierra.<br><br>
De forma conjunta, y otras en paralelo, tenemos a la comunidad de OpenStreetMap que se centra en mejorar y ampliar el mapa libre colaborativo mundial que es el centro de este proyecto.

-   Web: <https://geoinquietos.org>

## Comentarios

>La sala la dinamizaríamos desde el grupo Mapeado Colaborativo / Geoinquietos Zaragoza. Esta sala podría estar complementada con alguna actividad de mapeado en la mañana del domingo aún por definir.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
