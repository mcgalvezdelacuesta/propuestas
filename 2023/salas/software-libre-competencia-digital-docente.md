---
layout: 2023/post
section: proposals
category: devrooms
author: Comunidad Docente de Vitalinux
title: Software libre y Competencia Digital Docente
---

# Software libre y Competencia Digital Docente

## Detalles de la propuesta:

>Recientemente se ha publicado el Marco de Referencia de Competencia Digital Docente (MRCDD) en España, y el profesorado está en pleno proceso para acreditar su competencia digital docente. Muchas grandes tecnológicas están aprovechando para incrementar su presión sobre el sector educativo vinculando la utilización de sus herramientas a la acreditación de dicha competencia digital. Esta sala mostraría las posibilidades que ofrece el software libre en cuanto a poner a disposición aplicaciones educativas punteras y respetuosas con la privacidad del alumnado y el profesorado, tanto a las administraciones educativas como al profesorado en general.

-   Formato:

>Duración: viernes por la tarde y sábado por la mañana.<br><br>
Se trabajarían las 6 áreas descritas en el MRCDD. Para cada área se alternaría según su naturaleza la realización de una charla o taller. <br><br>
Por último cerraría una mesa redonda con discusión sobre la necesidad de la inclusión del software libre en las escuelas, como espacios libres de negocio.

-   Público objetivo:

>Administraciones educativas de diferentes comunidades autónomas que estén impulsando la formación y acreditación en competencia digital docente, y se planteen impulsar herramientas digitales a la disposición de su profesorado para ello.<br><br>
Profesorado en general que quiera mejorar su competencia digital docente utilizando herramientas social y éticamente comprometidas con la comunidad, y respetuosas con sus datos y con los de su alumnado.

<p style="padding-top: 5px; text-align: center; font-size: 1.5rem;"><strong>Pues enviar propuestas para esta sala desde <strong><a href="https://eslib.re/2023/enviar/sala-vitalinux" target="_blank">esta página</a></strong> hasta el <span class="red">19 de marzo</span>.<br></strong></p>

## Comunidad que propone la sala:

### Comunidad Docente de Vitalinux

> **Vitalinux EDU (DGA)** es la distribución Linux elegida por el Gobierno de Aragón para los centros educativos. Está basada en **Vitalinux**, que se define como un proyecto para llevar el Software Libre a personas y organizaciones facilitando al máximo su instalación, uso y mantenimiento.<br><br>
Alrededor de Vitalinux EDU se ha creado una comunidad de profesorado usuario de este sistema que ha visto y comprobado la potencialidad ética y técnica del software libre como herramienta digital para trabajar con el alumnado.

-   Web: <https://wiki.vitalinux.educa.aragon.es/index.php/P%C3%A1gina_principal>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
