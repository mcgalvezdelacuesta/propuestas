---
layout: 2023/post
section: proposals
category: devrooms
author: LibreOffice
title: LibreOffice
---

# LibreOffice

## Detalles de la propuesta:

>Encuentro de la comunidad LibreOffice

-   Formato:

>Charlas

-   Público objetivo:

>Usuario de LibreOffice, desarrolladores, voluntarios

## Comunidad que propone la sala:

### LibreOffice

> Comunidad LibreOffice en España

-   Web: <https://es.libreoffice.org>
-   Twitter: <https://twitter.com/@libreoffice>

## Propuestas confirmadas

- [Ismael Fanlo - ¡Festival de tablas dinámicas!](/2023/talleres/festival-tablas-dinamicas)
- [Juan C. Sanz - LibreOffice Base, una base de datos peculiar](/2023/charlas/libreoffice-base-base-datos-peculiar)
- [Mauricio Baeza - Mis primeras macros en LibreOffice, con Python](/2023/talleres/primeras-macros-libreoffice-python)
- [Juan C. Sanz - ¿Si tengo MySQL o MariaDB para qué necesito LibreOffice Base?](/2023/talleres/mariadb-para-que-necesito-libreoffice-base)

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
