---
layout: 2023/post
section: proposals
category: devrooms
author: Distros Linux España
title: Encuentro de Distribuciones Linux
---

# Encuentro de Distribuciones Linux

## Detalles de la propuesta:

>Espacio para compartir ideas, iniciativas, buenas prácticas en el desarrollo y mantenimiento de distribuciones linux desarrolladas o mantenidas. Para ello se puede dar cabida a: Ponencias, Talleres, mesas redondas o zonas de debate. Algunas propuestas iniciales: instaladores, versiones de base, entornos de escritorio, mantenimiento de aplicaciones, rolling-release...

-   Formato:

>Talleres, charlas y mesa redonda

-   Público objetivo:

>Personas con intereses en conocer y aportar soluciones e ideas al desarrollo de las diferentes distribuciones

<p style="padding-top: 5px; text-align: center; font-size: 1.5rem;"><strong>Pues enviar propuestas para esta sala desde <strong><a href="https://eslib.re/2023/enviar/sala-distros-linux" target="_blank">esta página</a></strong> hasta el <span class="red">19 de marzo</span>.<br></strong></p>

## Comunidad que propone la sala:

### Distros Linux España

>Grupo de responsables y técnicos de diferentes distribuciones Linux de España como AzLinux, Lliurex, Max, Linkat, Linex, Vitalinux...

-   Web: <https://distroslinux.catedu.es>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
