---
layout: 2023/post
section: proposals
category: talks
author: Eloy Crespo
title: Como proteger la privacidad de tus documentos con Collabora Online
---

# Como proteger la privacidad de tus documentos con Collabora Online

>Collabora Productivity es una empresa inglesa dedicada al desarrollo del producto Collabora Online. Es una suite ofimática 100% software libre basada en LibreOffice. En un primero momento, fuimos los responsables de la creación de LibreOffice Online. Todo lo que hacemos contribuye de vuelta al proyecto LibreOffice bajo MPLv2 License.<br><br> Actualmente trabjamos con más de 250 partners en todo el mundo para la integración de nuestra herramienta de ofimática en multitud de productos sofware libre. Siempre teniendo en mente ofrecer una alternativa a Google Docs y/o Office 365 que asegure la privacidad de los documentos y datos, la soveranía digital y evitar el vendor lock-in que la mayor partne de empresas de software libre tienen si adquieren productos de Microsoft.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>El objetivo sería presentar Collabora Online como una alternativa real a Google Docs y MS 365. Presentaré el proyecto, desde cómo nació la idea a través de LibreOffice y su comunidad, por qué el Software Libre es una alternativa real para asegurar la soberanía digital y privacidad de los datos y todos los beneficios relacionados con la adopción de este tipo de herramientas de ofimática en un entorno tanto público. Asimismo, presentar por qué empresas de toda Europa están adaptando nuestro producto Collabora Online como solución, para poder desarrollar sus propios productos basados en Software Libre intentando escapar del famoso "vendor lock-in" que los Google o Microsoft proponen como modelo de negocio.<br><br>
En añadido, soy el responsable de desarrollo de negoco en Collabora y da la casualidad que soy de Zaragoza. Es por ello que esta es una ocasión muy especial para mí al tratarse de un evento en mi ciudad. Estaría encantado en presentar nuestro producto y proyecto en mi ciudad :)<br><br>
Estoy a vuestra entera disposición para cualquier duda o cuestión adicional que pueda surgiros, así como para poder aportaros más información al respecto de nuestra empresa y proyecto.

-   Web del proyecto: <https://www.collaboraoffice.com/>

-   Público objetivo:

>Entidades educativas, organizaciones y administración pública y empresas.

## Ponente:

-   Nombre: Eloy Crespo

-   Bio:

>Eloy Crespo ha liderado el área de desarrollo de negocio y ventas de Collabora Productivity desde 2016. Orientado a ayudar a nuevos clientes de todos los tamaños, desde pequeñas hasta grandes empresas, a experimentar los beneficios de su propia suite de oficina privada en sus instalaciones o en las de un hoster local.<br><br>
Se especializa en ayudar a socios y clientes durante sus primeros pasos con Collabora, a desarrollar sus actividades comerciales basadas en Collabora Online y conectarlos con el resto de la empresa para una experiencia fluida.


### Info personal:

-   Mastodon (u otras redes sociales libres): <https://www.linkedin.com/in/eloy-crespo/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
