---
layout: 2023/post
section: proposals
category: talks
author: Arturo Martín Romero (Vitalinux)
title: Tuneando Entornos de Escritorio de manera remota y desatendida
---

# Tuneando Entornos de Escritorio de manera remota y desatendida

>Cómo personalizar diferentes aspectos del Entorno de Escritorio (usuarios, idioma, wallpaper, lanzadores, impresoras, horas de apagado, configuración de los navegadores Web o el comportamiento de aplicaciones) de manera remota y desatendida.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En la charla se pretende mostrar cómo personalizar a través de Migasfree los equipos que forman parte de una organización de forma individual o de manera agrupada, independientemente de su número, distribución y ubicación física.  Para ello se mostrará la estrategía seguida por Vitalinux para controlar los miles de equipos que forman parte de los distintos centros educativos de la comunidad de Aragón.

-   Web del proyecto: <https://wiki.vitalinux.educa.aragon.es>

-   Público objetivo:

>A cualquier organización que desee controlar de forma masiva, remota y desatendida sus equipos informáticos.

## Ponente:

-   Nombre: Arturo Martín Romero

-   Bio:

>Profesor de ciclos formativos de informática que actualmente se encuentra en atribución de funciones como asesor técnico informático del programa Vitalinux, encargado de su generación, gestión y mantenimiento.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://gitlab.vitalinux.educa.aragon.es>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
