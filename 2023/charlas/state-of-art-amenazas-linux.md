---
layout: 2023/post
section: proposals
category: talks
author: Paula de la Hoz
title: State of the art&#58; Amenazas en Linux
---

# State of the art: Amenazas en Linux

>La idea de la charla es repasar los ataque más relevantes de 2022 y primeros meses de 2023 que se han realizado contra varios tipos de sistemas de Linux. La charla pretende desmentir la idea de que Linux es seguro por defecto, pero al mismo tiempo proporcionar herramientas y conocimiento para afrontar las amenazas a las que se enfrenta.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La charla se presentaría en un formato accesible, pero incluyendo también detalles técnicos. Se analizarían las amenazas más comunes por un lado y las que han supuesto más impacto por otro. Se analizarán casos específicos, incluyendo una breve explicación de campañas descubertas a través de una honeypot personal. Explicaré cómo se investigan y correlacionan esta clase de ataques para mejorar la seguridad.

-   Público objetivo:

>SysAdmins de Linux interesados en seguridad o en general usuarios con interés técnico en ataques contra Linux.

## Ponente:

-   Nombre: Paula de la Hoz

-   Bio:

>Analista de ciberinteligencia en Sentinel One, previamente Threat Hunter, IR y Experta en ciberseguridad ofensiva. Ha trabajado de profesora de hardware hacking y pentesting en el FP de ciberseguridad Salesianos Atocha (Madrid). Presidenta cofundadora de la asociación Interferencias. Colaboradora del programa de radio social Post Apocalipsis Nau.

### Info personal:

-   Web personal: <https://www.inversealien.space>
-   Mastodon (u otras redes sociales libres): <https://mastodon.green/@alien>
-   GitLab (u otra forja) o portfolio general: <https://sr.ht/~alienagain/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
