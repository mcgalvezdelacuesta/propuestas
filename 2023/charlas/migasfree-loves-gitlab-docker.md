---
layout: 2023/post
section: proposals
category: talks
author: Eduardo Romero Moreno
title: Migasfree loves Gitlab&Docker
---

# Migasfree loves Gitlab&Docker

>Proceso de construcción y distribución de paquetes rpm para servidores Linux usando Gitlab, Docker y Migasfree.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En el Ayuntamiento de Zaragoza necesitamos crear nuestros propios paquetes rpm con utilidades internas para gestionar nuestro servidores Linux. Usamos Gitlab para almacenar el controlar las versiones del código y su integración continua para construir de manera automatizada rpms para los diferentes OS (Centos, OracleLinux, SLES o RedHat). A la par usamos Gitlab para trasladar los paquetes a Migasfree que es el encargado, entre otras cosas, de gestionar la disponibilidad de estos rpms según variables configurables.

-   Web del proyecto: <http://www.migasfree.org>

-   Público objetivo:

>Creación y distribución de código aplicado a la administración de paquetes en sistemas Linux.

## Ponente:

-   Nombre: Eduardo Romero Moreno

-   Bio:

>* Administrador de sistemas de Escritorio y Servidor Linux/Windows en el Ayuntamiento de Zaragoza desde el año 2000
* Master en SL por la URJC
* Integrante durante varios años del equipo AZLinux (Escritorio Linux en los puestos de trabajo del Ayuntamiento de Zaragoza)
* Colaborador en la iniciativa Vitalinux que lleva Escritorios Linux al sistema educativo aragonés

### Info personal:

-   Twitter: <https://twitter.com/@eduromo>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
