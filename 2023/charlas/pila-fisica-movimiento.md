---
layout: 2023/post
section: proposals
category: talks
author: Bibiana Noemí Boccolini
title: PILA para Física en movimiento&#58; Proyecto de Investigación libre y abierta
---

# PILA para Física en movimiento: Proyecto de Investigación libre y abierta

>El conocimiento libre es una responsabilidad educativa. Desde la UGPE (UNR) desarrollamos una investigación educativa cuyo objetivo principal es la construcción cooperativa de conocimiento libre en torno a la manera en que aprenden los estudiantes.<br><br>
**PILA para Física en movimiento** es una propuesta consistente en una secuencia de actividades y un BreakOut desarrollados con herramientas didácticas libres, para que los estudiantes resuelvan virtualmente mientras se indaga cómo aprenden temas de cinemática. Para ello, hemos conformado un equipo de docentes-investigadores que se encuentra en expansión.<br><br>
La idea de este trabajo es:
* demostrar cómo se construye conocimiento a partir de una investigación libre y abierta (ILA), descentralizada, responsable y ética que inspira la colaboración virtual
* propiciar el aprendizaje abierto como un bien común, a partir del trabajo con las comunidades educativas
* mostrar la experiencia que sirve para impulsar este formato y alentar su adopción

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español
-   Descripción:

>PILA para Física en movimiento es un Proyecto de Investigación Libre y Abierta (ILA) para indagar acerca del aprendizaje de contenidos de cinemática, en la Física.<br><br>
Para ello, desde la Unidad de Gestión de Proyectos Estratégicos (UGPE) de la Universidad Nacional de Rosario (UNR), Argentina, se elaboró una propuesta, basada en una secuencia didáctica de aprendizaje del contenido de movimiento rectilíneo uniforme (MRU) de cinemática, perteneciente a la asignatura Física, del plan de estudio de la escuela secundaria o bachillerato. Se complementa con un BreakOut, donde los estudiantes participan de una experiencia de aprendizaje basado en retos. Toda la propuesta se resuelve virtualmente y tiene una proyección de 30 horas reloj de aplicación.<br><br>
La propuesta es una investigación-acción educativa, de alcance colaborativo, para conocer si la intervención favorece el aprendizaje de contenidos de cinemática, si fomenta en el estudiante el gusto por la Física y su aprendizaje. Propone la realización de actividades que conducen a la explicación de principios, leyes y teorías, y la construcción de modelos que permitan predecir e interpretar los fenómenos físicos comunes.<br><br>
Hoy, la enseñanza de las ciencias tiene el deber ineludible de preparar al sujeto para la vida y esto se logra no solo proporcionando conocimientos básicos, sino desarrollando métodos y estrategias de aprendizaje que permitan la búsqueda del conocimiento a partir de situaciones problemáticas tomadas del entorno, para comprender la aplicación de la ciencia en la vida.<br><br>
La propuesta está a disposición de todos los docentes de Física que quieran aplicarla en sus clases, y decidan colaborar mediante el desempeño del rol de investigador educativo. Su función es la de analizar tanto el proceso de aprendizaje que realicen los estudiantes como el proceso de evaluación. Durante la aplicación de la propuesta, el estudio de campo es de tipo descriptivo, y sigue el marco metodológico de investigación-acción, pero basada en la ética y filosofía del conocimiento libre y abierto.<br><br>
Se basa en la filosofía del software y conocimiento libre y abierto. Aplica la filosofía y política de la investigación como bien común, de propiedad libre y colectiva, transparente y de acceso público sin barreras.<br><br>
Es libre porque el proceso y los productos se transparentan bajo una licencia que garantiza el uso libre por parte de los participantes y por otros miembros sin restricciones. Las publicaciones, los procesos y los instrumentos asociados son publicadas bajo esa licencia, contribuyendo con los carriles para la construcción del conocimiento libre.<br><br>
Constituye un camino para superar el problema de la apropiación del conocimiento y las publicaciones científicas con derechos de autor o patentes. La base conceptual y técnica invita a repensar el proceso de investigación con miras a nuevos formatos, dinámicos y activos.<br><br>
Acelera el desarrollo y la co-creación del conocimiento. Convocamos a profesores y agentes de educación que quieran sumarse a la valiosa experiencia de ser investigadores activos, libres y comprometidos con la mejora de la educación.

-   Público objetivo:

>Profesores de Física, profesores de Ciencias Naturales, directores de escuelas secundarias, supervisores de educación, estudiantes de la Formación del Profesorado de Educación Secundaria Obligatoria, Bachillerato y Formación Profesional en la especialidad de Física y Química, estudiantes de la Formación Docente en Ciencias Naturales / Física, profesores noveles de Ciencias Naturales / Física, investigadores sociales, padres comprometidos con la educación de sus hijos, público en general.

## Ponente:

-   Nombre: Bibiana Noemí Boccolini

-   Bio:

>Analista Universitaria de Sistemas (UTN, Argentina).Especialista Universitaria en Asesoramiento Didáctico e Intervención Educativa (UNED, España).Magister en Informática Educativa (UTEM, Chile).<br><br>
Profesora de la Tecnicatura Universitaria de Software Libre (UNL, Argentina) desde 2015.<br><br>
Profesora de Posgrado en universidades de Argentina: Maestrías de Educación Universitaria (UNR),Educación Artística (UNR),Educación Física (UNR) y Salud (UNCAUS) desde 2017.<br><br>
Directora de tesis en investigaciones basadas en la mediación con tecnologías educativas.<br><br>
Becaria del gobierno canadiense para la investigación de la Educación a Distancia (FEP,2002/2003) y el Teletrabajo (FRP 2010/2011).Coordinadora de Programas de la Asociación Argentina de Estudios Canadienses (ASAEC) desde 2009.<br><br>
Referente de Educación a Distancia del Ministerio de Educación de la Provincia de Santa Fe ante la Comisión Federal de Educación a Distancia,(2011 a 2016)
Evaluadora de artículos académicos de Revista HOLOGRAMÁTICA (UNLZ,Universidad Nacional de Lomas de Zamora,Arg) (desde 2020).<br><br>
Miembro del Proyecto AULAS LIBRES (Proyecto para la divulgación y uso del software libre en el campo educativo),coordinadora de capacitaciones a docentes y estudiantes en el diseño y uso de recursos libres y en la construcción colaborativa del conocimiento (desde 2008).<br><br>
Coordinadora de Proyectos de la UNIDAD DE GESTIÓN DE PROYECTOS ESTRATÉGICOS, dependiente del Rectorado de la Universidad Nacional de Rosario (UNR),desde 2020.<br><br>
Militante y divulgadora del software libre en ambientes educativos. Promueve investigaciones educativas libres y abiertas.<br><br>
Sus trabajos le valieron invitaciones a congresos,en Argentina y en el extranjero:
- RMLL (Rencontres Mondiales du logiciel libre) 2009-213-2017-2018 Francia
- EDUCODE, 2013-2018,Bélgica
- Colloque International en Éducation,2019,Canadá
- Colloque international du RIFEFF,2019,Argelia
- X Congreso Internacional Multidisciplinar de Investigación Educativa,2022, Barcelona, España

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
