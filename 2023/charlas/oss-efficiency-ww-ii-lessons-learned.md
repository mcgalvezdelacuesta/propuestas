---
layout: 2023/post
section: proposals
category: talks
author: Daniel Izquierdo
title: OSS and Efficiency&#58; World War II Lessons Learned
---

# OSS and Efficiency: World War II Lessons Learned

>Allá por 1944 la CIA generó un documento que daba instrucciones sobre cómo llevar a cabo sabotajes simples que cualquier ciudadano de a pie podía realizar. Curiosamente existe un apartado concreto enfocado a empresas de oficina y conferencias. La motivación es clara, si consigues que las empresas sean cada vez más ineficientes, la productividad general de tu enemigo decrecerá.<br><br>
Si volvemos a 2023, casi 80 años después de que dicho documento fuera escrito, veremos la vigencia de algunos puntos. Lo relevante es que el proceso de desarrollo de los proyectos de software libre y sus atributos básicos como la transparencia y la colaboración hacen que sean proyectos mucho más resilientes a posibles momentos adversos.<br><br>
En esta charla intentaré compartir cuáles son esos atributos básicos que hacen que el proceso de desarrollo de software libre sea un modelo difícil de sabotear y por lo tanto mucho más libre de riesgos que otras opciones.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Allá por 1944 la CIA generó un documento que daba instrucciones sobre cómo llevar a cabo sabotajes simples que cualquier ciudadano de a pie podía realizar. Curiosamente existe un apartado concreto enfocado a empresas de oficina y conferencias. La motivación es clara, si consigues que las empresas sean cada vez más ineficientes, la productividad general de tu enemigo decrecerá.<br><br>
Si volvemos a 2023, casi 80 años después de que dicho documento fuera escrito, veremos la vigencia de algunos puntos. Lo relevante es que el proceso de desarrollo de los proyectos de software libre y sus atributos básicos como la transparencia y la colaboración hacen que sean proyectos mucho más resilientes a posibles momentos adversos.<br><br>
En esta charla intentaré compartir cuáles son esos atributos básicos que hacen que el proceso de desarrollo de software libre sea un modelo difícil de sabotear y por lo tanto mucho más libre de riesgos que otras opciones.

-   Público objetivo:

>Cualquier persona interesada en charlar un rato sobre los aspectos básicos que hacen que los proyectos de software libre funcionen.

## Ponente:

-   Nombre: Daniel Izquierdo

-   Bio:

>Cofundador de Bitergia y actual CEO, Daniel es un apasionado de la minería de datos sobre proyectos de desarrollo de software. Daniel contribuye al proyecto CHAOSS (Community Health Analytics for Open Source Software) y es miembro de la junta del proyecto, así como de la junta de la Fundación InnerSource Commons.

### Info personal:

-   Twitter: <https://twitter.com/@dizquierdo>
-   GitLab (u otra forja) o portfolio general: <https://github.com/dicortazar>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
