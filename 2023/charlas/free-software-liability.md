---
layout: 2023/post
section: proposals
category: talks
state: canceled
author: Alexander Sander
title: Free Software and Liability
---

# Free Software and Liability

>In the EU, regulations on liability issues for Free Software are being discussed. The AI Act, the Product Liability Directive and the Cyber Resilience Act propose exemptions for Free Software. In my talk, I would like to shed light on the consequences for the Free Software ecosystem and the problems that arise from the reforms. In addition, I would like to venture an outlook on upcoming regulations and what role Free Software will play in the future.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: English

-   Descripción:
>In the EU, regulations on liability issues for Free Software are being discussed. The AI Act, the Product Liability Directive and the Cyber Resilience Act propose exemptions for Free Software. In my talk, I would like to shed light on the consequences for the Free Software ecosystem and the problems that arise from the reforms. In addition, I would like to venture an outlook on upcoming regulations and what role Free Software will play in the future.

-   Web del proyecto: <https://fsfe.org/>

## Ponente:

-   Nombre: Alexander Sander

-   Bio:

>FSFE Senior Policy Consultant

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mstdn.io/@lexelas>
-   Twitter: <https://twitter.com/@lexelas>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
