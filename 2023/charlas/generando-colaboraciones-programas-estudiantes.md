---
layout: 2023/post
section: proposals
category: talks
author: Carmen Delgado
title: Generando colaboraciones con programas de estudiantes
---

# Generando colaboraciones con programas de estudiantes

>Queremos compartir como hemos participados en varios programas y que hemos aprendido como comunidad de estas colaboraciones, así como motivar a otros proyectos a hacerlo.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Remoto
-   Idioma: Español

-   Descripción:

>De diciembre 2022 a marzo 2023 Eclipse Adoptium cuenta con dos "interns" dentro del programa Outreachy para que obtengan experiencia remunerada dentro de proyectos open-source, y así poder aprender sobre: buenas prácticas, colaboración, herramientas y darse a conocer, después de todas nuestra experiencia queremos compartir nuestras lecciones aprendidas en el proceso y porqué vemos valor en participar en esto.

-   Web del proyecto: <https://adoptium.net/> / <https://www.outreachy.org/>

-   Público objetivo:
> Personas que gestionan comunidades y proyectos open-source. Empresas que colaborar con proyectos para que puedan patrocinar a las vacantes disponibles.

## Ponente:

-   Nombre: Carmen Delgado

-   Bio:

>Desde octubre 2022 soy la community manager de Eclipse Adoptium desde la Eclipse Foundation, con experiencia en gestión de proyecto y operaciones de PYMEs, ONGs, y empresas tecnológicas en ámbitos que van desde salud, investigación clínica, fintech, desarrollo de software y ahora open-source. Desde el 2020 soy co-fundadora de una grupo de mujeres voluntarias que ayudamos a mujeres que están empezando en el sector tecnológico (Step4ward) creado dentro de la comunidad BCN Engineering, con Step4ward he participado en paneles y talleres en eventos en Barcelona y también hemos creado sesiones virtuales y desde que estoy en Adoptium también he participado en la creación de contenido de forma virtual para dar a conocer al comunidad.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/carmenldelgadop/>
-   Twitter: <https://twitter.com/@cldelgadop>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
