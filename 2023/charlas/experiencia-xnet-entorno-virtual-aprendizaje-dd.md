---
layout: 2023/post
section: proposals
category: talks
author: Sergio Salgado
title: La experiencia de Xnet con el entorno virtual de aprendizaje DD para una educación digital democrática y abierta
---

# La experiencia de Xnet con el entorno virtual de aprendizaje DD para una educación digital democrática y abierta

>A través de nuestra experiencia desarrollando e implementando una suite educativa que garantice derechos fundamentales del alumnado hemos identificado las siguientes problemáticas: la falta de enfoque en los derechos humanos y la democracia en la digitalización de la educación, la falta de formación en derechos digitales por parte de la administración pública, la falta de consideración de todos los actores involucrados en la educación digital, la necesidad de un enfoque integral de lo digital y la alfabetización digital, la importancia de la equidad y la inclusión en la educación digital, la necesidad de un enfoque crítico en la selección de tecnologías educativas y la importancia de la formación del profesorado en el uso crítico y ético de la tecnología.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:
>1. Proveer de herramientas de análisis a los centros educativos, el profesorado, la comunidad educativa, las y los legisladores y el público en general para entender, en primer lugar, el contexto y la cultura digital, y las problemáticas y oportunidades que ofrece en el contexto educativo, y para plantear, en segundo lugar, el diseño de pedagogías y de Estrategias Digitales de Centro desde una perspectiva innovadora, ágil, humanista y respetuosa con los derechos humanos.
2. Compartir el código público generado en Barcelona partiendo del Plan de Digitalización Democrática de la Educación de Xnet para agregar talento y generar oportunidad y crecimiento: presentación internacional de la herramienta DD de código público open source con la protección de los derechos digitales y la soberanía de datos de la comunidad educativa desde el diseño y por defecto. Actualmente en fase de implementación en 12 centros de la ciudad.

-   Web del proyecto: <https://xnet-x.net>

-   Público objetivo:

>Profesionales de la educación, de las TIC, de las TEP y familias implicadas en una educación democrática. Sociedad civil implicada en la lucha por una educación pública y preocupada por el desembarco de los grandes monopolios de internet en las escuelas.

## Ponente:

-   Nombre: Sergio Salgado

-   Bio:

>Consultor de comunicación y asesor política de diversas organizaciones. Implicado en múltiples proyectos activistas y presidente de Xnet, plataforma que trabaja en campos relacionados con los derechos digitales, la democracia en red, la lucha contra la corrupción y la defensa de Internet y su uso para la autoorganización y la transformación social. Xnet es el vivero del que surgen iniciativas como "Digital Democratico". Es coautor con Simona Levi de la obra "Hazte Banquero" y del libro "Votar y cobrar".

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://home.social/@Xnet>
-   Twitter: <https://twitter.com/@x_net_>

## Comentarios

- <https://elpais.com/espana/catalunya/2022-02-09/barcelona-crea-un-software-para-las-escuelas-alternativo-a-google-y-a-microsoft.html>
- <https://www.lavanguardia.com/vida/20220829/8490343/xnet-crea-declaracion-educacion-digital-democratica-abierta.html>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
