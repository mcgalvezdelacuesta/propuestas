---
layout: 2023/post
section: proposals
category: talks
state: canceled
author: Italo Vignoli
title: Sobre la sostenibilidad del software libre, el caso de LibreOffice
---

# Sobre la sostenibilidad del software libre, el caso de LibreOffice

>LibreOffice se anunció en 2010. Después de 10 años, era necesario revisar y actualizar la estrategia en función de la evolución del mercado de paquetes ofimáticos, para mejorar el modelo de sostenibilidad. Las empresas no están apoyando el proyecto tanto como los usuarios individuales. Con el tiempo, esto puede representar una amenaza para la sostenibilidad del proyecto. Hemos cambiado nuestra estrategia para educar a las empresas sobre el enfoque correcto del software libre, devolviendo para garantizar la sostenibilidad a largo plazo del proyecto LibreOffice.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>LibreOffice se anunció en 2010 para relanzar la innovación del ya estancado proyecto OpenOffice. Tras 10 años de crecimiento, era necesario revisar y actualizar la estrategia en función de la evolución del mercado de paquetes ofimáticos, para mejorar el modelo de sostenibilidad.<br><br>
De hecho, las empresas -aunque despliegan LibreOffice para ahorrar dinero respecto a las soluciones propietarias- no están apoyando el proyecto tanto como los usuarios individuales. Con el tiempo, esto puede representar una amenaza para la sostenibilidad, ya que se puede ralentizar el desarrollo.<br><br>
Por lo tanto, hemos cambiado nuestra estrategia para educar a las empresas sobre el enfoque correcto del software libre, devolviendo de una de las muchas formas disponibles: comprando la versión LTS del ecosistema, financiando el desarrollo de una función específica, pagando por la solución de un error o una regresión, etc.<br><br>
El enfoque correcto por parte de las empresas garantizaría la sostenibilidad a largo plazo del proyecto LibreOffice, y la evolución del formato de archivo estándar ISO Open Document Format (ODF) para una verdadera interoperabilidad.

-   Web del proyecto: <https://www.libreoffice.org>

## Ponente:

-   Nombre: Italo Vignoli

-   Bio:

>Italo Vignoli es miembro fundador de The Document Foundation y del proyecto LibreOffice, Presidente Emérito de la Associazione LibreItalia, Embajador del Patrimonio del Software y orgulloso miembro de la Free Software Foundation Europe (FSFE). Fue miembro de la Junta Directiva de la Iniciativa de Código Abierto (OSI).<br><br>
Italo codirige el marketing de LibreOffice, las relaciones públicas y con los medios de comunicación, copreside el Programa de Certificación de LibreOffice y es portavoz del proyecto. También se encarga de las actividades de promoción y marketing de la norma ISO Open Document Format.<br><br>
Ha contribuido a varios grandes proyectos de migración a LibreOffice, y es migrante y formador certificado de LibreOffice desde 2014. De 2004 a 2010 ha participado en el proyecto OOo.

### Info personal:

-   Web personal: <https://www.vignoli.org>
-   Twitter: <https://twitter.com/@italovignoli>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
