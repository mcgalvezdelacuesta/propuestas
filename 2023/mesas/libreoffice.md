---
layout: 2023/post
section: proposals
category: tables
author: LibreOffice
title: LibreOffice
---

## LibreOffice

-   Web: <https://libreoffice.org>

> Popular proyecto de ofimática libre, impulsado por The Document Foundation.

-   Twitter: <https://twitter.com/LibreOffice>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
