---
layout: 2023/post
section: proposals
category: workshops
author: Mauricio Baeza
title: Mis primeras macros en LibreOffice, con Python
---

# Mis primeras macros en LibreOffice, con Python

>Escribir macros en LibreOffice con Python con un conocimiento básico de Python y de desarrollo.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>El API de LibreOffice es uno de los más grandes de las aplicaciones de Software Libre. Dada su herencia de C++, tiene todavía bastantes áreas donde falta concisión y sencillez para su uso más claro. Con Python podemos hacer el camino más fluido para quien quiera probar todo el potencial de LibreOffice.

-   Web del proyecto: <https://doc.cuates.net/easymacro/es/>

-   Público objetivo:

>Cualquier usuario de LibreOffice o desarrollador de Python que quiera interactuar con LibreOffice.

## Ponente:

-   Nombre: Mauricio Baeza

-   Bio:

>Desarrollador de Software Libre. Miembro de la TDF (The Document Foundation). Expositor en las conferencias Latinoamericanas de LibreOffice Asunción, Paraguay 2019 y Brasilia, Brasil en 2022.
Bibilofilo, cinefilo, melomano...

### Info personal:

-   Web personal: <https://cuates.net>
-   Mastodon (u otras redes sociales libres): <https://mstdn.mx/@elmau>
-   GitLab (u otra forja) o portfolio general: <https://git.cuates.net/elmau>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
