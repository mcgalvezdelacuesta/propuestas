---
layout: 2023/post
section: proposals
category: workshops
author: Miguel Hernández Boza y Vicente J. Jiménez Miras
title: Introducción práctica a Runtime Security con Falco
---

# Introducción práctica a Runtime Security con Falco

>Este taller ofrece una introducción práctica a Falco, un proyecto de código abierto de la CNCF y la herramienta más empleada para la seguridad basada en llamadas al kernel. Se proporcionará a cada participante un sandbox (cluster de k3s) en el que aprenderán cómo instalar y configurar Falco para detectar y generar alertas de seguridad basadas en intrusiones en tiempo real.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>La adopción de contenedores y sistemas de orquestación de los mismos - como Kubernetes - se ha incrementado considerablemente en los últimos años. La popularidad de estas plataformas las convierte en objetivos comunes para cibercriminales. Kubernetes incluye herramientas de prevención, como los Admission Controllers, control de permisos RBAC y Pod Security Admission. Sin embargo, ésto se centra en la prevención y no proporciona gran visibilidad sobre las aplicaciones en tiempo de ejecución. ¿Cómo implementar un sistema de seguridad para detectar intrusiones en tiempo real en Linux, contenedores y/o Kubernetes? En este tutorial, los instructores presentarán qué es Runtime Security y explicarán cómo detectar comportamientos sospechosos e intrusiones.<br><br>
Este tutorial ofrece una introducción práctica a Falco, un proyecto de código abierto de la CNCF y la herramienta más empleada para la seguridad basada en llamadas al kernel. Se proporcionará a cada participante un sandbox (cluster de k3s) en el que aprenderán cómo instalar y configurar Falco para detectar y generar alertas de seguridad basadas en intrusiones en tiempo real.<br><br>
El tutorial cubrirá los siguientes puntos, de forma eminentemente práctica:
* Runtime Security y Falco - qué son y para qué sirven.
* Instalación y configuración.
* Entrada de datos para Falco - llamadas al sistema y otros plugins.
* Filtros - Reglas de Falco.
* Alertas - Canales de notificación (Syslog, Slack, etc.).<br><br>
Beneficios<br><br>
Los asistentes pueden conocer Falco, pero tal vez no lo hayan usado en entornos en producción. Este tutorial está enfocado a aquellos que quieran dar sus primeros pasos con Falco para detectar amenazas en tiempo de ejecución y alertar sobre intrusiones en Linux, contenedores y Kubernetes. La estructura es la siguiente:<br><br>
* Introducción a Runtime Security y conceptos básicos e instalación de Falco (teoría y lab, 30 minutos).
* Arquitectura de Falco y como activar, desactivar y extender reglas de detección (teoría y lab, 30 minutos).
* Notificaciones y visualización de alertas, integración con Slack (teoría y lab, 20 minutos)
* Cierre y preguntas (10 minutos).<br><br>
Al final de esta sesión, los asistentes serán capaces de responder a las siguientes preguntas:<br><br>
* ¿Qué es Runtime Security y por qué es importante?
* ¿Qué es Falco?
* ¿Cómo desplegar Falco en un cluster de Kubernetes para detectar amenazas?
* ¿Cómo emplear reglas de detección estándar para mi caso de uso particular?
* ¿Cómo detectar comportamientos sospechosos e intrusiones?
* ¿Cómo notificar sobre estos eventos de seguridad?

-   Web del proyecto: <https://falco.org/>

-   Público objetivo:

>El objetivo de esta sesión está enfocado a todos los públicos en general y, en particular, a aquellos con un interés en seguridad en entornos cloud que quieran ampliar sus conocimientos sobre Runtime Security y Falco.

## Ponente:

-   Nombre: Miguel Hernández Boza y Vicente J. Jiménez Miras

-   Bio:

>Miguel Hernández is a student for life with a passion for innovation. He has spoken at several cyber-security conferences, including HITB, RootedCon, TheStandoff, and Codemotion. He spent the last six years working in security research at big tech companies. In addition to contributing his own open-source projects. Currently, he’s a Security Content Engineer at Sysdig.<br><br>
Vicente J. Jiménez Miras has a background as an infrastructure engineer and open source trainer. Being a digital nomad since early in his career, he’s lived or worked in almost a hundred different locations. He also shares a passion for food, especially Indian cuisine, although, as a Spaniard, he’s very proud of his tortilla española.
In 2021, after 4 years working as an instructor for Red Hat, he joined Sysdig to continue his work educating technical teams and creating awareness of open source cybersecurity.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@miguelHzBz>
-   Twitter: <https://twitter.com/MiguelHzBz> / <https://twitter.com/vjjmiras>
-   GitLab (u otra forja) o portfolio general: <https://www.github.com/Miguel000>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
