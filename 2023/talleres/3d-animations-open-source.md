---
layout: 2023/post
section: proposals
category: workshops
author: Alvaro del Castillo San Felix
title: 3D Animations with Open Source
---

# 3D Animations with Open Source

>The 3D virtual worlds are more and more present in our lives. Creating them is a combination of several disciplines including character modelling, terrain creation, sound and music and of course, animation! In this workshop, the goal is to introduce in a practical way the basic workflows for animating these worlds using two Open Source stellar tools: Blender and Godot.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español


-   Descripción:

>During the workshop the basic steps for creating animated characters will be introduced:
* Modelling
* Rigging
* Animation<br><br>
The animation field is huge but in this workshop, a basic introduction will be covered with then idea to show the animation map, climb the initial growing curve so the attendees can start learning animation from a solid starting ground using Open Source tools.<br><br>
Blender is a really mature 3D creation suite, used for creating amazing animation films. After 20 years of development, blender has a huge community of developers and artists behind it and a Foundation to govern its evolution. It is a complex tool that has a high entry barrier but once it is broken, its 3D creation possibilities are limitless.<br><br>
Godot is the Open Source engine for creating 2D and 3D games. Its 4.0 version is pretty close to be released after 8 years of development, and it is the preferred tool by many game developer because its programming model based on independent scenes. It can import the models, rigging, animations and worlds created in Blender, and provides the perfect environment for making this worlds interactive so you can orchestrate what happens.<br><br>
At the end of this workshop, all the attendees must  be able to create a basic scene with animations in Blender, import it in Godot, and create an interactive experience.

-   Web del proyecto: <https://github.com/Voxelers/3d/projects/5>

-   Público objetivo:

>Developers and artists willing to learn howto create interactive 3d worlds with Open Source tools.

## Ponente:

-   Nombre: Alvaro del Castillo San Felix

-   Bio:

>Tech has been my passion during my lifetime, and applying them to new challenges a core motivation for me. Sharing this passion thanks to Open Source has been killer. My favorite time? Execution: design, coding and deploying with talented teams with the right technologies to achieve ambitious goals.

### Info personal:

-   Twitter: <https://twitter.com/acstw>
-   GitLab (u otra forja) o portfolio general: <https://github.com/acs>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
