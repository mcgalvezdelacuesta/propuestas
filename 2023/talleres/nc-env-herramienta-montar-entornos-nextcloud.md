---
layout: 2023/post
section: proposals
category: workshops
author: Pietro Marini
title: nc-env&#58; una herramienta para montar entornos Nextcloud en tu ordenador
---

# nc-env: una herramienta para montar entornos Nextcloud en tu ordenador

>Much@s de nosotr@s, con algo de conocimientos técnicos, ya tenemos y gestionamos una instalación de Nextcloud en nuestras casas o en nuestras oficinas. La puesta en servicio de una instancia básica no esta complicada, pero si queremos ofrecer un servicio completo y de calidad a nuestros usuarios, que aproveche plenamente de las funcionalidades ofrecidas por la plataforma, deseamos disponer de una herramienta que nos proporcione entornos de prueba que nos permiten validar nuestros escenarios de despliegue.<br><br>
nc-env es una herramienta de código abierto concebida para responder a esta exigencia y que no requiere recursos de computación en la nube.<br><br>
En este taller haremos una introducción, seguiremos los pasos de instalación y desplegaremos un entorno Nextcloud aprovechando unos de sus templates.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Much@s de nosotr@s, con algo de conocimientos técnicos, ya tenemos y gestionamos una instalación de Nextcloud en nuestras casas o en nuestras oficinas. La puesta en servicio de una instancia básica no esta complicada, pero si queremos ofrecer un servicio completo y de calidad a nuestros usuarios, que aproveche plenamente de las funcionalidades ofrecidas por la plataforma, deseamos disponer de una herramienta que nos proporcione entornos de prueba que nos permiten validar nuestros escenarios de despliegue.<br><br>
nc-env es una herramienta de código abierto concebida para responder a esta exigencia y que no requiere recursos de computación en la nube.<br><br>
En este taller haremos una introducción, seguiremos los pasos de instalación y desplegaremos un entorno Nextcloud aprovechando unos de sus templates.<br><br>
Puede resultar interesante consultar los siguiente recursos:
- programa del taller en esLibre 2022, [aquí](https://commons.wikimedia.org/wiki/File:EsLibre_2022_P24_-_Pietro_Marini_-_C%C3%B3mo_montar_un_cl%C3%BAster_de_Nextcloud.webm)
- articulo que expone el porque y el como (a alto nivel) de nc-env, [aquí](https://propuestas.eslib.re/2022/miscelanea/nc-env-creacion-entornos-nextcloud-local)

-   Web del proyecto: <https://codeberg.org/pmarini/nc-env/>

-   Público objetivo:

>Personas con conocimientos en entornos Linux y habilidades a linea de comando. Idealmente sysadmins o arquitectos encargados o interesados al mantenimiento y operaciones del aplicativo Nextcloud


## Ponente:

-   Nombre: Pietro Marini

-   Bio:

>Tras licenciarse en Física en 2011 en la Universidad de Pavía (Italia), Pietro ha trabajado como desarrollador y consultor en el sector de la Business Intelligence y la analítica de datos para empresas de toda Europa. En la actualidad vive con su familia en Galicia. Orientado al cliente, autodidacta y apasionado del mundo del software de código abierto, Pietro se ha incorporado al equipo Nextcloud GmbH en Junio 2021 y disfruta trabajando con los clientes para resolver los desafíos técnicos más complejos.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://codeberg.org/pmarini/>

## Comentarios

>- Duración taller: 2 horas
- Como el año pasado, me gustaría que hubiera un registro previo de los participantes. 10-15 participantes cómo máximo.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
