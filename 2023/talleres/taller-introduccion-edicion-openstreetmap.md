---
layout: 2023/post
section: proposals
category: workshops
author: Miguel Sevilla-Callejo
title: Taller de introducción a la edición en OpenStreetMap
---

# Taller de introducción a la edición en OpenStreetMap

>OpenStreetMap es el proyecto de datos espaciales libres por excelencia. También conocido como la Wikipedia de los mapas, y siguiendo el símil de esta, viene a constituir una base de datos espaciales colaborativa y de datos libres que están permitiendo crear el mayor y mejor mapa libre del mundo. Con este taller se pretende que cualquiera, desde cero, tenga unas primeras nociones de qué es el proyecto, cómo empezar a colaborar con él y formar parte de la comunidad.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>El taller llevaría un par de horas con una primera parte de introducción de OSM y otra para  que los participantes puedan hacer sus primeras ediciones una vez que se hayan dado de alta en el proyecto.

-   Web del proyecto: <https://wiki.openstreetmap.org>

-   Público objetivo:

>Cualquier persona interesada en compartir y editar datos espaciales libres de su barrio, su pueblo, su ciudad o el ámbito rural más próximo. No se requieren conocimientos especiales previos por lo que cualquier persona que se sienta cómoda con un ordenador puede contribuir a OpenStreetMap y asistir al taller.

## Ponente:

-   Nombre: Miguel Sevilla-Callejo

-   Bio:

>Soy doctor en Geografía, he trabajado en temas sobre desarrollo, mundo rural y la aplicación de tecnologías de la información geográfica en diferentes ámbitos, estuve de profesor asociado en el departamento de Geografía y Ordenación del Territorio de la Universidad de Zaragoza, soy asistente de investigación en el Instituto Pirenaico de Ecología del CSIC, y entre mis aficiones está la participación en las comunidades de software y datos espaciales libres, destacando mi presencia en la junta directiva de la asociación QGIS España, la implicación en la comunidad española de colaboradores de OpenStreetMap y la coordinación del grupo Mapeado Colaborativo / Geoinquietos Zaragoza.

### Info personal:

-   Web personal: <https://msevilla00.gitlab.io>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@msevilla00>
-   Twitter: <https://twitter.com/msevilla00>
-   GitLab (u otra forja) o portfolio general: <http://gitlab.com/msevilla00>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
