---
layout: 2021/post
section: proposals
category: devrooms
community: Laboratorio de la Sociedad Digital
title: EWOK Education With Open Knowledge
---

## Descripción de la sala

En esta sala se presentarán diferentes iniciativas de innovación docente que utilizan el software y el conocimiento libre como forma de mejora de la calidad educativa. La orientación principal del grupo que propone la sala es la innovación universitaria, pero estará abierta a propuestas de todos los niveles educativos, siempre que el cuerpo central sea una actividad que utilice alguna herramienta libre: software libre, creative commons, hardware libre, copyleft, Wikipedia, Open Street Maps o cualquier otra propuesta que entronque con esta filosofía. Sin ser un criterio excluyente, nos gustaría prestar especial atención a aquellas iniciativas que incluyen además un objetivo de compromiso social. Por nuestra parte, haríamos la presentación de un programa de editatones en Wikipedia, un mapatón para ayudar a la misión de Cruz Roja en Burundi utilizando Open Street Maps y la traducción colaborativa del libro Medieval Hackers. Es probable que tengamos alguna iniciativa propia más y reclutaremos más propuestas en nuestro entorno. Igualmente, estamos abiertos a propuestas externa

## Comunidad que la propone

#### Laboratorio de la Sociedad Digital

Somos un nuevo grupo de investigación de la Facultad de Tecnología y Ciencia de la Universidad Camilo José Cela.  

-   Web de la comunidad: <https://rafaelcondemelguizo.wixsite.com/laboratorio >
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/rcondemelguizo>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

### Contacto(s)

-   Nombre de contacto: Rafael Conde Melguizo
-   Email de contacto: <rconde@ucjc.edu>

## Público objetivo

Docentes e interesados en la educación.

## Formato

**Puedes encontrar el programa de esta sala en: <https://propuestas-ewok.eslib.re/2021/>**

Charlas cortas donde se presenten los diferentes proyectos con un tiempo final de reflexión, debate y, sobre todo, contactos para tejer redes de colaboración futuras.

## Comentarios
