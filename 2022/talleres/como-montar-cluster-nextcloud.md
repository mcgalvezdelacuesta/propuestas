---
layout: 2022/post
section: proposals
category: workshops
author: Pietro Marini
title: Cómo montar un clúster de Nextcloud
---

# Cómo montar un clúster de Nextcloud

> Nextcloud se está utilizando en producción en centenares de miles de organizaciones en todo el mundo, algunas de esas con bases de usuarios masivas.<br><br>
En este taller, de 2 horas de duración, montaremos un clúster Nextcloud básico utilizando la herramienta de código abierto nc-env, basada en vagrant y LXD. Al final de la sesión los asistentes tendrán una instalación en clúster de Nextcloud activa en sus equipos.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> Nextcloud se está utlizando en producción en centenares de miles de organizaciones en todo el mundo, algunas de esas con bases de usuarios masivas.<br><br>
En este taller, de 1 y media de duración, montaremos un clúster Nextcloud básico utilizando la herramienta de código abierto nc-env, basada en vagrant y LXD. Al final de la sesión los asistentes tendrán una instalación en clúster de Nextcloud activa en sus equipos.<br><br>
Para poder seguir activamente el taller, recomendamos que los asistentes lleven su ordenador con sistema operativo basado en Ubuntu (ej. Ubuntu Desktop 20.04) y que hagan el setup inicial del entorno, cuya instrucciones serán disponibles unas semanas antes del evento en este enlace<br><br>
<strong>IMPORTANTE: </strong>Revisar la wiki que detalla todas las informaciones sobre el taller:<br><br>
<a href="https://codeberg.org/pmarini/nc-env/wiki/Taller-esLibre-2022" target="_blank">https://codeberg.org/pmarini/nc-env/wiki/Taller-esLibre-2022</a>

-   Web del proyecto: <https://codeberg.org/pmarini/nc-env/>

-   Público objetivo:

> Personas con conocimientos en entornos Linux y habilidades a linea de comando. Idealmente sysadmins encargados o interesados al mantenimiento y operaciones del aplicativo Nextcloud

## Ponente:

-   Nombre: Pietro Marini

-   Bio:

> Tras licenciarse en Física en 2011 en la Universidad de Pavía (Italia), Pietro ha trabajado como desarrollador y consultor en el sector de la Business Intelligence y la analítica de datos para empresas de toda Europa. En la actualidad vive con su familia en Galicia. Orientado al cliente, autodidacta y apasionado del mundo del software de código abierto, Pietro se ha incorporado al equipo Nextcloud GmbH en Junio 2021 y disfruta trabajando con los clientes para resolver los desafíos técnicos más complejos.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
