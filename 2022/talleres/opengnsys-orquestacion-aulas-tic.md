---
layout: 2022/post
section: proposals
category: workshops
author: Jose María Guisado Gómez y Javier Sánchez Parra
title: OpenGnsys 1.2&#58 Orquestación para aulas TIC de enseñanza
---

# OpenGnsys 1.2: Orquestación para aulas TIC de enseñanza

> OpenGnsys es un software de orquestación para el mantenimiento y despliegue de imágenes de sistemas operativos en aulas TIC de enseñanza.<br><br>
Pequeña introducción a OpenGnsys y a nuestra visión sobre el proyecto con la intención de dar a conocerlo para crear una comunidad de usuarios y contribuidores.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> OpenGnsys es un sistema para facilitar el despliegue de sistemas operativos en aulas TIC de enseñanza. OpenGnsys se emplea en universidades públicas y privadas en España. En esta ponencia se detallarán los componentes principales del ecosistema y los esfuerzos que se han llevado recientemente en el proyecto en su versión OpenGnsys 1.2. También hablaremos sobre la visión del futuro del proyecto.
* Introducción al problema y como OpenGnsys los soluciona
* Origen de OpenGnsys
* Arquitectura y componentes de OpenGnsys
* Futuro de OpenGnsys
* Despliegue y uso básico de OpenGnsys<br><br>
Realizaremos una demostración en vivo mostrando el proceso de
instalación y todas funcionalidades que posee OpenGnsys.<br><br>
Por la limitación de tiempo, no podremos asistir a los asistentes que
quieran seguir el proceso con sus equipos.

-   Web del proyecto: <https://opengnsys.es>

-   Público objetivo:

> Técnicos de mantenimiento de aulas TIC de enseñanza

## Ponente:

-   Nombre: Jose María Guisado Gómez y Javier Sánchez Parra

-   Bio:

> Jose y Javier son desarrolladores de software de Sevilla, España. Miembros del grupo de software libre SUGUS GNU/Linux y contribuidores de varios proyectos, entre ellos OpenGnsys.

### Info personal:

-   Twitter: <https://twitter.com/opengnsys>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/opengnsys>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
