---
layout: 2022/post
section: proposals
category: workshops
author: Karla L Hernandez
title: Inkscape, diseño vectorial para no diseñadores
---

# Inkscape, diseño vectorial para no diseñadores

> En mi tiempo libre desde el 2011 migre del uso de la suite de adobe a herramientas libres para diseño desde entonces utilizo inkscape para todos los diseños vectoriales que realizo en los activismos sociales que apoyo y a los cuales dedico tiempo desde entonces, como pasatiempo comence en el diseño para impresos para apoyar en casa con la economia familiar ahora tengo mucha experiencia y no temo compartir todo lo que se al momento en diseño vectorial de personajes y afiches para diverso contenido digital e impreso.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Remoto

-   Descripción:

> El taller es una dinamica que de ir desde lo esencial hasta lo mas complejo en diseño grafico vectorial abordamos un poco de historia, tecnisimos sobre la herramienta lo suficiente para tener algun criterio solido sobre diseño vectorial, diferencias entre cuando usar y no usar una herramienta vectorial, como migrar en diversos formatos un vector y como incrustar metadata de autoria en el vector. Algunas recomendaciones sobre formato y fuentes dependiendo de si sera un documento para usar en web o en una impresión.<br><br>
No busco hacer pensar que vamos a sustituir el conocimiento de un graduado en diseño grafico pero para personas con bajo presupuesto o con el simple gusto de querer saber usar inskcape para algun proyecto personal del cual no cuenta con mayor presupuesto que si mismo para iniciar.

-   Público objetivo:

> Personas que no son diseñadoras interesadas en aprender a usar una herramienta de diseño vectorial para generar personajes o afiches para web y/o impresos.

## Ponente:

-   Nombre: Karla L Hernandez

-   Bio:

> Tecnóloga en el desarrollo de herramientas libres por profesión y dedicada al activismo feminista de tiempo completo. De ambas surge la sinergia de ser miembro fundadora de comunidades de tecnologias como Open Hardware y Geek girls latam El Salvador y miembro activa de otras comunidades como wikimedia user group El Salvador, Mozilla Hispano y Comunidad Centroamericana de Software Libre.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/karlalhernandez/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
