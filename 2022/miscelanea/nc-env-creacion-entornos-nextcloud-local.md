---
layout: 2022/post
section: proposals
category: misc
author: Pietro Marini
title: A tool to deploy Nextcloud Environments in your laptop
---

# Nc-env: A tool to deploy Nextcloud Environments in your laptop

Author: Pietro Marini, Senior Sales Engineer, Nextcloud GmbH

> **_NOTE:_**  A workshop on nc-env is scheduled on June 24 during EsLibre 2022. Chech [this page](https://propuestas.eslib.re/2022/talleres/como-montar-cluster-nextcloud) for more information!

## Introduction

When I joined the Sales Engineering team at Nextcloud in June 2021, I soon realized that I needed a tool enabling me to quickly and simply deploying a Nextcloud server in my laptop.

Not only that. What I also needed, was a way to run several independent Nextcloud instances that wouldn't leave any trace after their deletion and wouldn't imply installing software on my laptop that I wouldn't need afterwords.

As a hands-on person, I thought that such a tool would have been of great help for a Sales Engineer who needs to do product demonstrations, perform troubleshooting and show new features to prospects and customers on a very frequent basis.

Some weeks into the role, I then found out the huge variety of Nextcloud deployment options that touch all the layers of a typical Nextcloud architecture: Web Server, Database, Cache, Storage as well as other services such document online editing, video-conferencing, Full Text Search.

By keeping all this into account, I came up with the idea of building a tool that would enable me to quickly provision simple or complex Nextcloud environments in my laptop, that wouldn't have any side effect on my host operating system and would be highly resource-efficient. With some pre-existing experience with Vagrant and LXD, I decided to use those tools and created my first nc-env template.

## How it works

So how does nc-env work? You will need to setup the system by installing Vagrant and LXD plus some additional utility (detailed step-by-step instructions are available for several Linux distributions [here](https://codeberg.org/pmarini/nc-env/src/branch/main/how-to)) and you will pick the template or the templates that you need to provision your target environment.

As an example, let's say you want to create an environment with a Nextcloud server, latest release, with Nextcloud Office, which requires a Collabora Online server. You can scan through the list of available templates [here](https://codeberg.org/pmarini/nc-env#available-templates) to see that you'll need to provision the following templates: `template01-nextcloud-standalone` and `template02-collabora-code`.

For each template you'll need to follow the provisioning process:

1. Copy the template in its dedicated folder.
2. Adapt the files `Vagrantfile` and `provision.sh` to your needs. The instructions are available in a `Readme.md` file available for all the templates.
3. Put all the required artifacts in the  folder `artifacts`.
4. Run: `vagrant up`.
5. Wait.. and use your service(s)!

In this example you would repeat this procedure for both templates and then you will just need to do the final graphical configuration steps to connect Nextcloud with Collabora Online.

Once you are done and your environment is not needed anymore you can delete the containers by issuing the `vagrant delete -f` command from the container folder.

## Benefits

As stated above, two key requirements for the tool are that it must be highly resource-efficient and flexible. Resource efficiency is guaranteed by using LXD containers that, while featuring a similar experience as virtual machines have a much smaller footprint, almost irrelevant when they are idle. Flexibility is guaranteed by both tools, LXD and vagrant, that offer very easy management (creation, operation and deletion) of live containers.

Moreover LXD offers plenty of functionalities that make it easier to manage a large number of containers: you can organize them in projects, you can define several networks with custom firewall rules, you can define several storage backends, you can limit the hardware resources that one or group of containers can use, and more. As an example, you can simulate a complex network topology by defining different network interfaces, firewall and routing rules and assigning these interfaces to the containers that belong into the network.


## Conclusion

nc-env is meant as a tool to simplify the task of anyone who needs to deploy, use and test simple or complex Nextcloud environments. It does so by leveraging only your local devices resources so avoiding all the drawbacks of cloud environments and making use of two mature and well-know open source projects, LXD and Vagrant.

In the enterprise, it can boost support and other technical departments in charge of operating and maintaining Nextcloud environments, especially Service Providers.

For the home user, it is a handy tool to try new features and integrations without risking a disruption on the main Nextcloud instance.

To get started just [install it](https://codeberg.org/pmarini/nc-env/src/branch/main/how-to), pick your template and spin up your first container!

## Contributing

The project is open source under the GNU GPL License and hosted at Codeberg. You can find the code repository [here](https://codeberg.org/pmarini/nc-env).

The main areas of work are: existing templates maintenance, new templates development and container provsioning process simplification.

Feel free to open an issue in the repository should you have any advice, propose a feature or find a bug.
