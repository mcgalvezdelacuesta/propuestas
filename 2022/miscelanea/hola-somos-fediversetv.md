---
layout: 2022/post
section: proposals
category: misc
author: FediverseTV
title: Hola, somos FediverseTV
---

# Hola, somos FediverseTV

> FediverseTV (FTV) es una comunidad construida alrededor de Peertube, que es un software de servidor que facilita la construcción de plataformas para la publicación y emisión de vídeos y lo hace de manera federada y descentralizada. Permite subir vídeos, hacer directos y que estos se compartan  automáticamente  con  comunidades que consideramos amigas, lo cual dificulta la censura y mejora la disponibilidad de estos.

## Detalles de la propuesta:

-   Tipo de propuesta: Póster

-   Descripción:

> FediverseTV (FTV) es una comunidad construida alrededor de Peertube, que es un software de servidor que facilita la construcción de plataformas para la publicación y emisión de vídeos y lo hace de manera federada y descentralizada. Permite subir vídeos, hacer directos y que estos se compartan  automáticamente  con  comunidades que consideramos amigas, lo cual dificulta la censura y mejora la disponibilidad de estos.<br><br>
Instancia es cada instalación de este código en un servidor y con un dominio propio, decimos que es una red   federada porque cada una de estas instancias puede federar con otras, es decir pueden mostrar sus contenidos a otras y estas pueden mostrar los suyos a la primera a su vez, todas las instancias son iguales entre si y cada una pone sus reglas en su espacio.<br><br>
La comunidad de FTV nació con el fin de albergar contenido afín a nuestra ideología anticapitalista, antirracista y transfeminista. Por ello, ya que nos relacionamos con otras comunidades simpatizantes, tenemos mucho cuidado con cuales si o no permitimos estas relaciones, ya que queremos mantener nuestra ideología. No es una instancia dedicada a un contenido en concreto, se puede subir de cualquier tipo, siempre y cuando no incumpla el manifiesto de convivencia.<br><br>
En definitiva y abreviando… FTV es una plataforma para subir vídeos y hacer directos que federa con otras instancias de Peertube  y otros programas, que en su conjunto se conocen como Fediverso.<br><br>
No podremos estar en el evento, pero si alguna persona quiere contactar con nosotros puede hacerlo en:
* Nuestro correo: <hola@fediverse.tv>
* Nuestro Mastodon: <https://mastodon.art/@fediversetv>
* Sala de Matrix: <https://matrix.to/#/#peertubevideos:matrix.org>
* Nuestro Blog (donde publicamos noticias, eventos, y donde está prácticamente toda la información del colectivo): <https://blog.fediverse.tv/>

-   Web del proyecto: <https://fediverse.tv>

-   Público objetivo:

> A todo el mundo que quiera/necesite subir contenido de algún proyecto personal y sobretodo para colectivos de todo tipo.

## Ponente:

-   Nombre: FediverseTV
-   Enlace al póster en Wikimedia Commons: <https://commons.wikimedia.org/wiki/File:Cartel_FediverseTV.png>

### Info personal:

-   Web personal: <https://blog.fediverse.tv>
-   Mastodon (u otras redes sociales libres): <https://mastodon.art/@fediversetv>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
