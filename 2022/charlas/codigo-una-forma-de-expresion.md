---
layout: 2022/post
section: proposals
category: talks
author: María del Mar Diez Henao
title: ¿Es el código una forma de expresión? ¿Debe ser protegido bajo el derecho a la libertad de expresión? ¿Cuáles serían las consecuencias de ésto?
---

# ¿Es el código una forma de expresión? ¿Debe ser protegido bajo el derecho a la libertad de expresión? ¿Cuáles serían las consecuencias de ésto?

> El código se escribe principalmente para comunicarse con las máquinas. Bernstein contra Departamento de Justicia de EEUU es un caso histórico que resultó en el establecimiento del código como discurso y cambió las regulaciones de exportación de los Estados Unidos sobre software de encriptación, allanando el camino para el comercio electrónico internacional.<br><br>
Sin embargo las consecuencias de este caso aún están por determinar, por ejemplo, ¿está el FBI violando los derechos de la Primera Enmienda de los empleados de Apple en sus esfuerzos por extraer datos del iPhone de un terrorista? Y sobre todo... ¿Cuál es la situación en Europa?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial

-   Descripción:

> La charla se enfocará en las consecuencias legislativas de recoger el código como forma de expresión y busca crear un debate sobre qué limites se deben imponer, qué consecuencias puede tener que se proteja el código informático y los datos como expresión y qué limites a la libertad de expresión en este campo podrán ser impuestos.
La exposición comparará el marco jurídico americano y español.

-   Público objetivo:

> Apasionados del software

## Ponente:

-   Nombre: María del Mar Diez Henao

-   Bio:

> Asesora legal en NoLegalTech, especializándonse en derecho de la ciberseguridad, privacidad de los datos y blockchain en la Universidad de Indiana.

### Info personal:

-   Twitter: <https://twitter.com/@MarDiezHenao97>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
