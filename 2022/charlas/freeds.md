---
layout: 2022/post
section: proposals
category: talks
author: Lolo y Pablo ZG
title: FreeDs
---

# FreeDs

> FreeDS es el resultado de la unión de un hardware y un software con el objetivo de conseguir un gestor de excedentes universal y totalmente independiente de la fuente que genere nuestros excedentes (ya sean excedentes de generación fotovoltaica y/o eólica), de esta manera se puede sacar provecho de este excedente de producción que no se este usando para consumos principales y derivarlo hacia una carga resistiva.<br><br>
Aunque el objetivo principal de FreeDs es que sea universal (y de hecho lo es), además también dispone de modos de funcionamiento específicos para ciertos inversores solares (Solax, Fronius, SMA, Victron, Ingeteam, goodwe, etc.) lo que le da aún mas versatilidad al proyecto.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> ¿Cómo funciona?<br><br>
El principio de funcionamiento es relativamente sencillo, se mide la cantidad de energía que no se esta consumiendo (excedente), bien de forma universal con un medidor de potencia (meter) o bien de forma especifica obteniendo los datos a través del inversor u otra fuente de datos soportada y una vez obtenida la potencia sobrante, esta se deriva con una placa de control de potencia (pwm / dimmer) hacia la carga resistiva (como por ejemplo un termo para calentar agua sanitaria, una estufa o placa de calor).<br><br>
Debido a que los excedentes de producción son variables por múltiples factores, el proceso de medida de excedente y su posterior derivación se realizan cíclicamente y de forma continua en el tiempo, obteniéndose así un ajuste coordinado entre el excedente y la potencia derivada.<br><br>
Modos de funcionamiento<br><br>
La forma original y principal forma de obtención de datos de este proyecto es usando un hardware universalmente llamado "meter", en este modo la toma de datos (excedente) se realiza con un medidor de potencia (meter) que habrá que instalar en el lugar adecuado para que haga las mediciones de forma correcta (más adelante detallaremos en que lugar y la manera correcta de hacerlo).<br><br>
Adicionalmente existen los modos de funcionamiento de conexión a inversor o medidores externos (que detallaremos mas adelante).<br><br>
Detalles<br><br>
Toda la documentación del proyecto esta en abierto publicada en GitHub y existe un grupo de Telegram en el que contamos con más de 1700 personas activas actualmente en el que se crea comunidad y asesoramos en el montaje, soldado y emsamblaje de FreeDs.<br><br>
ES NECESARIO REGISTRARSE PARA ASISTIR A ESTE TALLER POR CUESTIONES DE DISPONIBILIDAD DE RECURSOS: <a href="https://eventos.librelabgrx.cc/events/cbabd201-7850-4ad0-b919-1f916cc61326" target="_blank">https://eventos.librelabgrx.cc/events/cbabd201-7850-4ad0-b919-1f916cc61326</a>

-   Web del proyecto: <http://freeds.es/>

-   Público objetivo:

> Personas con una instalación solar fotovoltaica o que estén pensado en montarla que quieran tener un control de los excedentes para derivarlos hacía de producción y una motorización de los mismos, con esto podemos derivar con una placa de control de potencia (pwm / dimmer) hacia la carga resistiva (como por ejemplo un termo para calentar agua sanitaria, una estufa o placa de calor).

## Ponente:

-   Nombre: Lolo y Pablo ZG

-   Bio:

>  
- Lolo (Manuel Caño-García) es un joven apasionado de la electrónica, deporte y la vida. Nació en Frailes (Jaén) y estudió en Granada (Física e Ingeniería electrónica). Desde muy pequeño e inconscientemente estuvo ligado al software libre. En casa, el ordenador con debían, se quedaba sin interfaz gráfica que había que levantar cada vez que se quería jugar a algún juego. Hoy en día es profesor en la UPM, e investiga en nanotecnología.
- Pablo ZG, es funcionario público con titulación superior en ingeniría informática, en sistemas de control y en robótica.

### Info personal:

-   Web personal: <https://about.me/amcalo>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/pablozg/freeds/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
