---
layout: 2022/post
section: proposals
category: talks
author: Raster
title: Backups sencillos en GNU/Linux con Cronopete
---

# Backups sencillos en GNU/Linux con Cronopete

> Las copias de seguridad son un elemento fundamental para cualquier usuario. Por desgracia, lo más habitual es no tenerlas. En esta charla quiero presentar Cronopete, una herramienta de copias de seguridad orientada hacia el usuario medio.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> ¿Cuantas veces has perdido datos importantes por culpa de un disco duro que "se muere"? ¿Cuantas veces te has tirado de los pelos porque esa copia que tenías era de hace varios meses y es completamente inservible?<br><br>
Hacer copias de seguridad es una tarea aburrida que todos hemos rehuido, pero es completamente necesaria en un mundo digital como el actual. Por eso, contar con una herramienta de backup sencilla de utilizar y de la que uno "se pueda olvidar" es fundamental.

-   Web del proyecto: <https://www.rastersoft.com/programas/cronopete_es.html>

-   Público objetivo:

> Usuarios de GNU/Linux "de escritorio".

## Ponente:

-   Nombre: Raster

-   Bio:

> Ingeniero de telecomunicación a tiempo parcial y linuxero a tiempo completo desde 1999. Tengo varios proyectos de software libre orientados al usuario medio.

### Info personal:

-   Web personal: <https://www.rastersoft.com>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/rastersoft>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
