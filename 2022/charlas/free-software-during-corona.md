---
layout: 2022/post
section: proposals
category: talks
author: Alexander Sander
title: What role did Free Software play during the corona crisis?
---

# What role did Free Software play during the corona crisis?

> The Corona crisis has made it clear to us which developments in digitisation we have missed in recent years. Administrations, but also many employers, were not prepared for this. Often, they simply chose some “obvious” solution in order to remain or become workable. Only a few considered the effects and consequences, for example in terms of data protection and privacy, as well as future dependencies on single vendors.<br><br>
At the same time, there are also positive examples. Free software has become more and more of a topic and more and more administrations have understood the advantages it brings.<br><br>
I would like to take a look at the developments of the past years and give an outlook on how we can better face future crises through the use of Free Software and what role governments, public bodies and administrations and the principle of “Public Money? Public Code!” play in this.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> The EU has an “Open Source Strategy”, the World Health Organisation supports Free Software solutions to tackle the crisis and more and more governments across Europe commit to Free Software. Is this the beginning of a new era or just a flash in the pan? What has been implemented so far and what can be expected in the future? Will the principle “Public Money? Public Code!” become reality soon? The talk will discuss important decisions and developments from the past and address these questions.<br><br>
This talk will be connected with a Workshop I want to host together with my colleague Lina Ceballos (see other submission: Workshop on Public Money? Public Code! - contact your administration).

-   Web del proyecto: <https://publiccode.eu/>

-   Público objetivo:

> Beginners, Free Software lovers, civil servants

## Ponente:

-   Nombre: Alexander Sander

-   Bio:

> Alexander has studied politics in Marburg, Germany and later has been an MEP Assistant in Brussels for three years and the General Manager of Digitale Gesellschaft e.V. in Berlin for four years. Furthermore he is the founder of NoPNR!, a campaign against the retention of travel data. He now works as a Policy Consultant for the Free Software Foundation Europe.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mstdn.io/@lexelas>
-   Twitter: <https://twitter.com/@lexelas>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
