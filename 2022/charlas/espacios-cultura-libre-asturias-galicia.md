---
layout: 2022/post
section: proposals
category: talks
author: Mariah Villarreal
title: Los espacios de cultura libre en Asturias y Galicia
---

# Los espacios de cultura libre en Asturias y Galicia

> Presento los resultados de mi investigación del máster de historia y análisis sociocultural sobre los espacios de cultura libre. El objetivo es identificar y investigar los espacios de los grupos de cultura libre dentro de Asturias y Galicia para añadir al fondo de conocimiento sobre la historia y cultura de este movimiento. Existe investigaciones sobre cultura libre en España pero falta el enfoque en Asturias y Galicia en concreto. Esta investigación no solo intenta desarrollar el estudio de los grupos de cultura libre en estas provincias, también intentará enfocarse en la construcción de espacios para estas comunidades.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Esta investigación es mi pequeño intento de desarrollar el cuerpo de conocimiento sobre más factores de la cultura libre basados en el análisis de los espacios que producen, ocupan y usan.<br><br>
No existen estudios de los espacios de cultura libre en Asturias y Galicia en concreto. Hay estudios sobre las características, comportamientos y motivaciones de los participantes de otros lugares dentro de España, pero no hay ninguna investigación que intente enfocarse en los espacios de la cultura libre. Hay estudios sobre “makerspaces” y “hackerspaces” y sobre la estética de los objetos digitales de comunidades en la red. Aunque no hay mucho enfoque en como producen los espacios digitales o cuales valores son presente en los espacios digitales o físicos.<br><br>
La investigación está basada mayormente en métodos cualitativos. Es un trabajo de campo con colección de datos a base de entrevistas, encuestas y observaciones dentro de participación en las comunidades. Además, hay un enfoque en fuentes primarias como los sitios web en que participan y que desarrollan las comunidades para describir los espacios en linea.<br><br>
Usando el mapa del artículo “Piensa globalmente, actúa localmente: mapeo de la cultura libre en un sistema mediático híbrido” de Calvo, puede identificar los grupos activos de cultura libre en Asturias y Galicia. A partir de esta identificación, contactaré con los miembros de los grupos y visitaré los sitios que tengan para hacer entrevistas, encuestas y trabajo de campo. Para colectar los datos necesarios, realizaré una muestra aleatoria de estos miembros entre los meses de febrero y abril. Durante este tiempo, participé en eventos en linea y presenciales, cuando los había, para observar los espacios y como interactúan los participantes. Es importante coleccionar historias orales también y compararlas con los materiales que producen estos grupos.<br><br>
Tengo experiencia previa como participante y investigadora en la cultura libre hace nueve años.

-   Web del proyecto: <https://www.mariahnoelle.org/espacioslibres>

-   Público objetivo:

> Personas que participan en grupos de cultura libre en Asturias y Galicia,
etnógrafos, antropólogos, historiadores.

## Ponente:

-   Nombre: Mariah Villarreal

-   Bio:

> Soy Mariah. Me apasiona el aprendizaje y la libertad del software. Actualmente, trabajo como contratista con las Boston Public Schools, manteniendo Canvas, un sistema de gestión de aprendizaje de software libre para un programa de preparación de profesores. Mis logros recientes incluyen la enseñanza de informática en la Academia Margarita Muñiz, una escuela pública bilingüe en Boston, así como la directora de Libre Learn Lab, una conferencia que reúne a hackers, educadores y legisladores para discutir recursos de licencia libre para entornos de aprendizaje. Mi activismo tiene sus raíces en la comunidad, la justicia social, y la justicia restaurativa.<br><br>
En julio, espero terminar el máster de historia y análisis sociocultural en la Universidad de Oviedo.

### Info personal:

-   Web personal: <https://www.mariahnoelle.org>
-   Mastodon (u otras redes sociales libres): <https://octodon.social/@noemu>
-   Twitter: <https://twitter.com/@MariahNoelle>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
