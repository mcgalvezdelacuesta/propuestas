---
layout: 2020/post
section: proposals
category: talks
title: Futuro de esLibre y de la comunidad del software libre
---

Este será un rato para reflexionar sobre qué queremos hacer con esLibre, y con la comunidad que le da sentido: la comunidad del software libre. Si te interesan, ¡vente!

## Formato de la propuesta

Desconferencia (unconference)

## Ponente(s)

No hay ponentes como tales, pero un par de humanos actuarán como animadores:

* JJ Merelo, Universidad de Granada
* Jesús M. González Barahona, Universidad Rey Juan Carlos
