---
layout: 2020/post
section: proposals
category: talks
title: Un bot (no inteligente) multi-interfaz y distribuido como asistente personal
---

La web está fracasando; cada vez es más difícil y costoso acceder a la información en un mar de entornos pesados, publicidad y otras dificultades. Sin embargo internet es una fuente inagotable de conocimiento y los que lo proporcionan deberían ayudar a sus lectores/usuarios a conseguir la información de la mejor manera posible (para los usuarios, no para ellos).

La línea de instrucciones sigue siendo una interfaz valiosa, pero está poco adaptada a las formas de acceso y conectividad más habituales (fundamentalmente dispositivos móviles). Se propone un bot (no inteligente: no hay procesamiento de lenguaje natural ni tampoco actividades con inteligencia), distribuido y ligero para acceder a la información disponible en la red en un formato conveniente, adaptable a nuestros diversos entornos de trabajo y a las necesidades personales de acceso a la información.

## Formato de la propuesta

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

[Errbot](https://errbot.readthedocs.io/) es un `chatbot` libre que permite conectarse a diversos servicios de mensajería y programar respuestas ante diversas peticiones. La infraestructura permite escribir extensiones (`plugins`) para añadir funcionalidades. Desde nuestro punto de vista tenía una limitación porque sólo puede atender a conversaciones en un sistema de mensajería lo que limitaba su versatilidad a la hora de utilizarlo en diversos contextos (escritorio, en movilidad, ...) y estilos de uso. Esto es, se puede elegir la interfaz pero el chatbot sólo es capaz de escuchar en un canal.

Se presentará:

-   La idea general de funcionamiento de Errbot
-   La extensibilidad proporcionada mediante el mecanismo de extensiones (`plugins`)
-   Una extensión que permite a varios de estos bots desplegados en diversas localizaciones pasar peticiones a otros para obtener diversos resultados. Se utilizan mecanismos de 'Mando y Control' (`Command and Control`)
-   Los programas siguen siendo importantes, pero en el contexto en el que se desarrollan los sistemas actualmente también lo son las APIs (Interfaces Programables de Aplicación, `Application Programmable Interfaces`) que son las que permiten desarrollos como el que se presenta

## Público objetivo

Curiosos, desarrolladores y personas que quieran tener el control sobre qué ven, cómo y cuándo.

## Ponente(s)

**Fernando Tricas García**: soy profesor del [Departamento de Informática e Ingeniería de Sistemas de la Universidad de Zaragoza](https://webdiis.unizar.es/~ftricas/), además de las participaciones en congresos de investigación, puedo aportar mi participación en algunas charlas sobre software libre:

-   2012-04-18: Algunas ideas sobre software libre en la [Entrega del Premio Local del VI Concurso Universitario de Software Libre](http://osluz.unizar.es/content/entrega-del-premio-local-del-vi-concurso-universitario-de-software-libre)
-   2005-08-07: Ponencia en las II Jornadas Tecnológicas Andaluzas de Software Libre "Comunidades Virtuales y Conocimiento Libre", Mollina, Málaga.

### Contacto(s)

-   **Fernando Tricas García**: ftricas at unizar dot es

## Comentarios

Probablemente sólo pueda acudir a uno de los dos días de las Jornadas.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
