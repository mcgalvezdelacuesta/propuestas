---
layout: 2020/post
section: proposals
category: talks
title: Software Heritage&#58 una infraestructura común para preservar nuestro software comunitario
---

## Formato de la propuesta

Presentación plenaria

## Descripción

El software está en el corazón de nuestra sociedad digital, y encarna una parte cada vez mayor de nuestro conocimiento científico, técnico y organizativo. [Software Heritage](softwareheritage.org/) es una iniciativa sin ánimo de lucro cuya misión es asegurar que este precioso cuerpo de conocimiento sea preservado a lo largo del tiempo, y se mantenga a disposición de toda la humanidad.

Hacemos esto por muchas razones. Para preservar el conocimiento científico y tecnológico que hay en el código fuente del software. Para permitir un mejor desarrollo y reutilización del código por la sociedad y la industria. Para promover una ciencia mejor, construyendo la infraestructura de preservación, compartición y referencia de software científico, paso fundamental para la reproducibilidad, y complemento necesario a la publicación abierta. Para afrontar el riesgo de pérdida masiva de código fuente desarrollado por la comunidad del software libre, cuyos sitios de alojamiento de código se cierran si su popularidad decrece.

Hemos recogido ya más de 8.000 millones de ficheros únicos, de más de 140 millones de repositorios, y los hemos organizado en un grafo de Merkle enorme, con deduplicación completa para todos los repositorios.

Convocamos a todo el mundo, desde individuos hasta empresas, desde entidades públicas hasta privadas, a que contribuyan activamente a esta misión.


## Ponente

Roberto Di Cosmo.

After obtaining a PhD in Computer Science at the University of Pisa, Roberto Di Cosmo was associate professor for almost a decade at Ecole Normale Supérieure in Paris, and became a Computer Science full professor at University Paris Diderot in 1999. He is currently on leave at Inria. He has been actively involved in research in theoretical computing, specifically in functional programming, parallel and distributed programming, the semantics of programming languages, type systems, rewriting and linear logic. His main focus is now on the new scientific problems posed by the general adoption of Free Software, with a particular focus on static analysis of large software collections, that were at the core of the european reseach project Mancoosi. Following the evolution of our society under the impact of IT with great interest, he is a long term Free Software advocate, contributing to its adoption since 1998 with the best-seller Hijacking the world, seminars, articles and software. He created the Free Software thematic group of Systematic in October 2007, and since 2010 he is director of IRILL, a research structure dedicated to Free and Open Source Software quality. In 2016, he co-founded and directs Software Heritage, an initiative to build the universal archive of all the source code publicly available.
