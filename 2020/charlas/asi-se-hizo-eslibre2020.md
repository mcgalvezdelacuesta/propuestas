---
layout: 2020/post
section: proposals
category: talks
title: Así se hizo esLibre 2020
---

esLibre 2020 iba a realizarse presencialmente en el campus de Fuenlabrada de la URJC. Pero finalmente se ha tenido que realizar de forma virtual. Para que esto haya sido posible, ha habido que evaluar y desplegar varias herramientas, repensar completamente el modelo del congreso, interaccionar con todas las personas implicadas (colaboradores, ponentes, organizadores de salas). Esta presentación explicará sobre todo la parte técnica de este proceso: cómo se ha montado un congreso virtual utilizando de forma prácticamente única software libre:

* Cómo hemos usado GitLab para la recogida de propuestas y la gestión del sitio web y otros flujos de información. Esto ya se hizo en 2019, y ha habido que adaptarlo sólo muy superficialmente, pero es un caso interesante de cómo se consigue una proceso bastante ligero utilizando herramientas habituales en el desarrollo de software.

* Cómo se evaluaron Jitsi, BigBlueButton y otras herramientas que pudieran ayudar en la videoconferencia, y RocketChat y otras herramientas para la mensajería instantánea. Cómo se han desplegado, y cómo están funcionando hasta el momento. Toda esta infraestructura es nueva, debido a las circunstancias de esta edición virtual. Se detallará también sobre qué plataforma (hardware, red) está funcionando.

* Cómo se han preparado las salas en Mozilla Hubs, para el evento social de esLibre 2020, y cómo podrías crear las tuys propias.

## Formato de la propuesta

Ponencia invitada.

## Ponente(s)

- Germán Martínez, Interferencias/LibreLabGRX
- Antonio Gutierrez, URJC
- Andrea Villaverde, URJC
- Jesús M. González Barahona, URJC
