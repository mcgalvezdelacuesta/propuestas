---
layout: 2020/post
section: proposals
category: devrooms
title: Perl, Raku y otros lenguajes de scripting
---

Perl es un lenguaje que sigue siendo en muchos casos el que mantiene la infraestructura web de la organización o con el que trabajas para administración avanzada de sistemas; Raku es su sucesor designado, y es un lenguaje potente y expresivo que pretende seguir vigente en los próximos 100 años. Ambos cuentan con una comunidad diversa y apasionada de personas comprometidas con la libertad del software.

No hay muchos eventos de esta comunidad a lo largo del año; sólo "Barcelona Perl & Friends" sigue manteniendo la llama viva. Esta sala pretende ser una alternativa para el encuentro de esta comunidad en nuestro país.

Adicionalmente, Perl y Raku mantienen mucho en común con otros lenguajes de programación como Ruby, PHP, e includo Python o Lua, o programación en AWK o shell o lenguajes de scripting de la primera generación. Esta sala se abre a todos ellos, para compartir experiencias y aprender unos de otros.

Se trata tanto de ponencias relacionadas directamente con el lenguaje o casos de uso, como de metodologías que se puedan aplicar a los mismos, en general, mejor si con casos específicos en esos lenguajes, pero no necesariamente.

## Comunidad o grupo que lo propone

JJ Merelo, fundador de **Granada Perl Mongers** y miembro también de los Madrid y Barcelona Perl Mongers; desarrollador de Raky y desarrollador en Perl durante más de 25 años.

### Contactos

-   **Juan Julián Merelo Guervós**: jjmerelo at gmail dot com | @JJ5

## Público objetivo

A cualquier desarrollador en estos dos lenguajes o a quien le interese la forma de hacer las cosas con ellos, así como a desarrolladores de otros lenguajes que quieran aprender juntos o compartir experiencias. También está abierto a gente que trabaje con bibliotecas o servicios que quiera contar cómo se accede y qué aportan al lenguajes.

## Tiempo

En principio, medio día.

## Día

Preferimos en sábado.

## Formato

La DevRoom incluirá un taller de Raku, porque todavía es un lenguaje emergente, y también de Perl, si se desea. Aparte, se hará una llamada para ponencias que seguirá el mismo formato que esLibre, a base de merge requests sobre un repositorio de GitLab específico.

## Comentarios

Daremos pegatinas.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x]  Acepto coordinarme con la organización de esLibre.
-   [x]  Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
